#!/usr/bin/env sucrase-node

import { transcode } from "./index"

const filesGlob = ["*/*.mkv", "!*/*HEVC*", "!_Finished"]

;(async () => {
  try {
    console.log(filesGlob)
    await transcode("anime", filesGlob, { match: "glob" })
  } catch (err) {
    console.error(err)
    process.exit(2)
  }
})()
