import { FfmpegTranscoder } from "./ffmpeg"
import execa from "execa"
import { extname, basename, join, dirname } from "path"

export class HevcTranscoder extends FfmpegTranscoder {
  name = "HEVC"
  additionalFlags = "-c:v libx265".split(" ")

  async exclude(path: string) {
    const p = await execa("ffprobe", [
      ..."-v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1".split(
        " "
      ),
      path
    ])
    return p.stdout.includes("hevc")
  }

  rename(path: string): string {
    return join(
      dirname(path),
      basename(path, extname(path)) + "[HEVC]" + this.outputExtname(path)
    )
  }

  protected outputExtname(path: string) {
    let outExtname = extname(path)
    if (outExtname === ".gif") {
      outExtname = ".mkv"
    }
    return outExtname
  }
}
