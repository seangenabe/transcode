#!/usr/bin/env -S npx --package sucrase sucrase-node

import { HevcTranscoder } from "./hevc"
import { AnimeTranscoder } from "./anime"
import { stat, unlink } from "fs/promises"
import { Rav1eTranscoder } from "./rav1e"
import { Transcoder } from "./transcoder"
import chalk from "chalk"
import globby from "globby"
import ora from "ora"
import prettyBytes from "pretty-bytes"
import prettyMs from "pretty-ms"
import trash from "trash"
import { ComicTranscoder, PhotoTranscoder, WebpTranscoder } from "./webp"
import { SvtAv1Transcoder } from "./svt-av1"
import { OpusTranscoder } from "./opus"
import { OpusAnyTranscoder } from "./opusany"
import { AvifEncoder } from "./avif"
import cpFile from "cp-file"
import { ok } from "assert"
import minimist from "minimist"
import Ajv from "ajv"
import S from "fluent-json-schema"
import { file as temporaryFile } from "tempy"
import { basename, extname, join, resolve } from "path"
import { SvtAv1CompatTranscoder } from "./svt-av1-compat"
import { existsSync } from "fs"

const modes = {
  hevc: new HevcTranscoder(),
  anime: new AnimeTranscoder(),
  rav1e: new Rav1eTranscoder(),
  webp: new WebpTranscoder(),
  comic: new ComicTranscoder(),
  photo: new PhotoTranscoder(),
  svtav1: new SvtAv1Transcoder(),
  svtav1compat: new SvtAv1CompatTranscoder(),
  opus: new OpusTranscoder(),
  opusany: new OpusAnyTranscoder(),
  avif: new AvifEncoder()
} as const

const _m: Record<string, Transcoder> = modes
ok(_m)

export async function* transcode(
  mode: keyof typeof modes,
  globs: string[],
  {
    source = "try-trash",
    temp = "use",
    match = "glob",
    destinationSelector = { type: "auto" },
    cwd = process.cwd()
  }: {
    source?: "delete" | "keep" | "trash" | "try-trash" | "try-delete"
    temp?: "use" | "skip"
    match?: "glob" | "exact"
    destinationSelector?:
      | { type: "auto" }
      | { type: "same-file" }
      | { type: "directory"; directory: string }
    cwd?: string
  }
) {
  const spinner = ora()
  let files: string[]
  spinner.info(`globs ${JSON.stringify(globs)}`)
  spinner.info(`cwd ${JSON.stringify(cwd)}`)
  if (match === "exact") {
    files = globs
  } else {
    files = await globby(globs, { cwd })
  }
  spinner.info(`files ${JSON.stringify(files)}`)
  files = files.filter(Boolean)

  if (files.length === 0) {
    spinner.succeed("No input files.")
    yield {
      type: "fileProcessed",
      result: "skipped",
      source,
      temp
    }
    return
  }

  const transcoder = modes[mode]
  if (!transcoder) {
    throw new Error(`Unknown mode: ${mode}`)
  }
  const globalStart = Date.now()
  let totalReduction = 0
  let totalOldFileSize = 0

  for (let path of files) {
    path = resolve(cwd, path)

    if (await transcoder.exclude(path)) {
      spinner.info(`Skipping ${path}.`)
      yield {
        type: "fileProcessed",
        result: "skipped",
        sourcePath: path,
        destinationPath: path,
        source,
        temp
      }
      continue
    }
    const progress = (s: string) =>
      spinner.start(`Transcoding ${path} to ${transcoder.name}. ${s}`)

    spinner.start(`Transcoding ${path} to ${transcoder.name}.`)
    const start = new Date()

    let end: Date
    let newFileSize: number
    let oldFileSize: number

    try {
      let destination: string
      if (destinationSelector.type === "auto") {
        destination = transcoder.rename(path)
      } else if (destinationSelector.type === "same-file") {
        ok(source === "delete", "source must be delete")
        ok(temp === "use", "temp must be use")
        destination = path
      } else if (destinationSelector.type === "directory") {
        destination = join(
          destinationSelector.directory,
          basename(transcoder.rename(path))
        )
      } else {
        ok(false)
      }

      // Determine transcode process destination (could be a temporary file)
      let transcodeDestination
      if (temp === "use") {
        transcodeDestination = temporaryFile({
          extension: extname(destination)
        })
      } else if (temp === "skip") {
        transcodeDestination = destination
      } else {
        ok(false)
      }

      // Check if destination already exists
      if (existsSync(destination) && destination !== path) {
        spinner.fail(
          "Destination path already exists. Assuming destination path to be a sucessful result"
        )
        yield {
          type: "fileProcessed",
          result: "skipped",
          sourcePath: path,
          destinationPath: destination,
          source,
          temp
        }
        continue
      }

      // TRANSCODE FILE
      await transcoder.transcode(path, transcodeDestination, { progress })

      end = new Date()

      // Check if output file is larger.
      newFileSize = (await stat(transcodeDestination)).size
      oldFileSize = (await stat(path)).size
      if (newFileSize > oldFileSize) {
        spinner.fail(`Skipping ${path}, output file is larger.`)
        yield {
          type: "fileProcessed",
          result: "skipped",
          sourcePath: path,
          destinationPath: path,
          source,
          temp
        }
        continue
      }

      if (source === "delete") {
        if (source !== destination) {
          // Delete source file
          await unlink(path)
        }
      } else if (source === "trash") {
        // Trash source file
        await trash(path)
      } else if (source === "try-trash") {
        // Try trash source file
        try {
          await trash(path)
        } catch (err) {
          spinner.warn((err as any).message)
        }
      } else if (source === "try-delete") {
        // Try delete source file
        try {
          await unlink(path)
        } catch (err) {
          spinner.warn((err as any).message)
        }
      }

      if (temp === "use") {
        // Copy temporary destination file to intended destination
        await cpFile(transcodeDestination, destination)
        await unlink(transcodeDestination)
      }

      yield {
        type: "fileProcessed",
        result: "done",
        sourcePath: path,
        destinationPath: destination,
        source,
        temp
      }
    } catch (err) {
      spinner.fail(`Failed to transcode ${path}.`)
      spinner.fail((err as any).message)
      continue
    }

    ok(end!)
    ok(oldFileSize!)
    ok(newFileSize!)

    const durationStr = prettyMs(Number(end) - Number(start))
    const reduction = oldFileSize - newFileSize
    totalReduction += reduction
    totalOldFileSize += oldFileSize
    const reductionStr = prettyBytes(reduction)
    const reductionPercent = Math.floor((reduction * 100) / oldFileSize)

    spinner.succeed(
      `Done transcoding ${chalk.blue(path)} in ${chalk.blue(
        durationStr
      )}. Reduced file size by ${chalk.yellow(reductionStr)} (${chalk.yellow(
        String(reductionPercent)
      )}%) from ${chalk.red(prettyBytes(oldFileSize))} to ${chalk.green(
        prettyBytes(newFileSize)
      )}.`
    )
  }
  const globalEnd = Date.now()
  const totalNewFileSize = totalOldFileSize - totalReduction
  const totalReductionPercent = Math.floor(
    (totalReduction * 100) / totalOldFileSize
  )

  if (totalOldFileSize > 0) {
    spinner.succeed(
      `Total time spent: ${chalk.blue(
        prettyMs(globalEnd - globalStart)
      )} Reduced total file size by ${chalk.yellow(
        prettyBytes(totalReduction)
      )} (${chalk.yellow(String(totalReductionPercent))}%) from ${chalk.red(
        prettyBytes(totalOldFileSize)
      )} to ${chalk.green(prettyBytes(totalNewFileSize))}.`
    )
  } else {
    spinner.info("No files processed.")
  }
}

if (require.main === module) {
  const ajv = new Ajv()
  const validate = ajv.compile(
    S.object()
      .prop("temp", S.enum(["skip", "use"]))
      .prop(
        "source",
        S.enum(["keep", "delete", "trash", "try-trash", "try-delete"])
      )
      .prop("match", S.enum(["glob", "exact"]))
      .valueOf()
  )
  ;(async () => {
    try {
      const args = minimist(process.argv.slice(2))

      const validateResult = validate(args)
      if (!validateResult) {
        console.error(ajv.errorsText(validate.errors))
        throw new Error("validation failed")
      }
      const {
        temp,
        source,
        match,
        _: [mode, ...globs]
      } = args

      if (!mode) {
        throw new Error("mode not specified")
      }
      if (globs.length === 0) {
        throw new Error("globs not specified")
      }

      for await (const _ of transcode(mode as keyof typeof modes, globs, {
        temp,
        source,
        match
      })) {
      }
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
  })()
}
