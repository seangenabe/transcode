import { Transcoder } from "./transcoder"
import { extname, basename, dirname, join } from "path"
import execa = require("execa")

export class AvifEncoder implements Transcoder {
  name = "AVIF"
  exclude(path: string) {
    return extname(path) === ".avif"
  }
  async transcode(source: string, destination: string): Promise<void> {
    await execa("avifenc", [
      ..."-j 16 -s 0 -a cq-level=10 -a tune=ssim".split(" "),
      source,
      destination
    ])
  }
  rename(path: string): string {
    return join(dirname(path), basename(path, extname(path)) + ".avif")
  }
}
