import { join, dirname, basename, extname } from "path"
import { FfmpegTranscoder } from "./ffmpeg"
import execa = require("execa")

export class Rav1eTranscoder extends FfmpegTranscoder {
  name = "AV1 via rav1e"
  additionalFlags =
    "-c:v librav1e -tile_rows 6 -tile_columns 4 -sc_detection true -speed 4 -qp 80".split(
      " "
    )

  async exclude(path: string) {
    const p = await execa("ffprobe", [
      ..."-v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1".split(
        " "
      ),
      path
    ])
    return p.stdout.includes("av1")
  }

  rename(path: string): string {
    return join(
      dirname(path),
      basename(path, extname(path)) + "[AV1]" + this.outputExtname(path)
    )
  }

  protected outputExtname(path: string) {
    let outExtname = extname(path)
    if (outExtname === ".gif") {
      outExtname = ".mkv"
    }
    return outExtname
  }
}
