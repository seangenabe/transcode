import { FfmpegTranscoder } from "./ffmpeg"
import { join, extname, dirname, basename } from "path"
import execa from "execa"

export class SvtAv1Transcoder extends FfmpegTranscoder {
  name = "AV1 via SVT-AV1"
  additionalFlags =
    "-c:v libsvtav1 -crf 23 -preset 6 -g 240 -svtav1-params tune=0:scd=1".split(" ")

  async exclude(path: string) {
    const p = await execa("ffprobe", [
      ..."-v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1".split(
        " "
      ),
      path
    ])
    return p.stdout.includes("av1")
  }

  rename(path: string): string {
    return join(
      dirname(path),
      basename(path, extname(path)) + "[AV1]" + this.outputExtname(path)
    )
  }

  protected outputExtname(path: string) {
    let outExtname = extname(path)
    if (outExtname === ".gif") {
      outExtname = ".mkv"
    } else if (outExtname === ".webm") {
      outExtname = ".webm"
    }
    return outExtname
  }
}
