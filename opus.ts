import { Transcoder, TranscodeOptions } from "./transcoder"
import { extname, basename, join, dirname } from "path"
import execa = require("execa")

export class OpusTranscoder implements Transcoder {
  name = "Opus"

  exclude(path: string): boolean {
    return extname(path) === ".opus"
  }

  async transcode(
    source: string,
    destination: string,
    { flags = [], progress = () => {} }: TranscodeOptions = {}
  ) {
    const p = execa("opusenc", [
      ..."--vbr --comp 10".split(" "),
      ...flags,
      source,
      destination
    ])
    await p
  }

  rename(path: string): string {
    return join(dirname(path), basename(path, extname(path)) + ".opus")
  }
}
