#!/usr/bin/env -S npx --package sucrase sucrase-node

import { Channel, connect, ConsumeMessage, Options } from "amqplib"
import { from } from "ix/asynciterable"
import { PassThrough } from "stream"
import { connectionUrl, queueName } from "./connection"
import { setTimeout } from "timers/promises"

async function run() {
  const connection = await connect(connectionUrl)
  try {
    const channel = await connection.createChannel()

    await channel.assertQueue(queueName)

    channel.prefetch(1)

    console.log("Consuming incoming tasks.")

    for await (const message of consumeUntilCancelled(channel, queueName)) {
      try {
        const { filename } = JSON.parse(message.content.toString("utf-8")) as {
          filename: string
        }

        console.log("Transcoding", filename)

        const startTime = new Date()
        let now = new Date()

        do {
          const interval = Number(now) - Number(startTime)
          console.log(`It's been ${Math.floor(interval / 60000)} minutes`)
          await setTimeout(30000)
          now = new Date()
        } while (Number(now) - Number(startTime) < 3600000)

        channel.ack(message)
        console.log("Transcoding done")
      } catch (err) {
        console.error("Error occurred in task.")
        console.error(err)
        // throw message
        channel.nack(message, false, false)
      }
    }
  } finally {
    await connection.close()
  }
}

;(async () => {
  try {
    await run()
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()

function consumeUntilCancelled(
  channel: Channel,
  queue: string,
  options?: Options.Consume
) {
  const s = new PassThrough({ objectMode: true, highWaterMark: 1000 })
  channel.consume(
    queue,
    (message) => {
      if (message == null) {
        s.end()
      } else {
        s.write(message)
      }
    },
    options
  )
  return from<ConsumeMessage>(s)
}
