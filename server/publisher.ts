#!/usr/bin/env -S npx --package sucrase sucrase-node

import { connect } from "amqplib"
import { resolve } from "path"
import { setTimeout } from "timers/promises"
import { connectionUrl, queueName } from "./connection"

const cwd = process.cwd()

async function run() {
  const [, , filename] = process.argv

  if (!filename) {
    throw new Error("No filename")
  }

  const resolvedFilename = resolve(cwd, filename)

  const connection = await connect(connectionUrl)
  try {
    const channel = await connection.createChannel()

    await channel.assertQueue(queueName)

    channel.sendToQueue(
      queueName,
      Buffer.from(JSON.stringify({ filename: resolvedFilename }), "utf-8"),
      {
        persistent: true
      }
    )
    console.log("Enqueued file.")

    await setTimeout(500)
  } finally {
    await connection.close()
  }
}

;(async () => {
  try {
    await run()
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()
