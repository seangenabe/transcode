#!/usr/bin/env -S npx --package sucrase sucrase-node

import { Channel, connect, ConsumeMessage, Options } from "amqplib"
import execa from "execa"
import { from } from "ix/asynciterable"
import { PassThrough } from "stream"
import { transcode } from ".."
import { connectionUrl, queueName } from "./connection"

async function run() {
  const connection = await connect(connectionUrl)
  try {
    const channel = await connection.createChannel()

    await channel.assertQueue(queueName)

    await channel.prefetch(1)

    console.log("Consuming incoming tasks.")

    for await (const message of consumeUntilCancelled(channel, queueName)) {
      try {
        const { filename } = JSON.parse(message.content.toString("utf-8")) as {
          filename: string
        }

        console.log("Transcoding:", filename)

        const p = await execa("ffprobe", [
          ..."-v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1".split(
            " "
          ),
          filename
        ])

        if (p.stdout.includes("av1") || p.stdout.includes("hevc")) {
          console.log("Skipping, already encoded in AV1 or HEVC")
          channel.nack(message, false, false)
        } else {
          if (
            ["mp4", "mkv", "avi"].some((ext) => filename.endsWith(`.${ext}`))
          ) {
            if (filename.match(/\bAnime\b/)) {
              await transcode("anime", [filename], { match: "exact" })
            } else {
              await transcode("svtav1compat", [filename], { match: "exact" })
            }
            channel.ack(message)
          } else {
            console.log("Skipping non-video")
            channel.nack(message, false, false)
          }
        }

        console.log("Done:", filename)
      } catch (err) {
        console.error("Error occurred in task.")
        console.error(err)
        // throw message
        channel.nack(message, false, false)
      }
    }
  } finally {
    await connection.close()
  }
}

;(async () => {
  try {
    await run()
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
})()

function consumeUntilCancelled(
  channel: Channel,
  queue: string,
  options?: Options.Consume
) {
  const s = new PassThrough({ objectMode: true, highWaterMark: 1000 })
  channel.consume(
    queue,
    (message) => {
      if (message == null) {
        s.end()
      } else {
        s.write(message)
      }
    },
    options
  )
  return from<ConsumeMessage>(s)
}
