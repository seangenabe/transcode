import { Transcoder, TranscodeOptions } from "./transcoder"
import { extname, basename, join, dirname } from "path"
import execa = require("execa")

export class WebpTranscoder implements Transcoder {
  name = "WebP"

  protected additionalFlags: string[] = []

  exclude(path: string): boolean {
    return extname(path) === ".webp"
  }

  async transcode(
    source: string,
    destination: string,
    { flags = [], progress = () => {} }: TranscodeOptions = {}
  ) {
    const p = execa("cwebp", [
      "-o",
      destination,
      ..."-m 6 -progress -metadata all".split(" "),
      ...this.additionalFlags,
      ...flags,
      "--",
      source
    ])
    await p
  }

  rename(path: string): string {
    return join(dirname(path), basename(path, extname(path)) + ".webp")
  }
}

export class ComicTranscoder extends WebpTranscoder {
  additionalFlags = "-preset drawing".split(" ")
}

export class PhotoTranscoder extends WebpTranscoder {
  additionalFlags = "-preset photo".split(" ")
}
