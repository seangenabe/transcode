import execa from "execa"
import { Transcoder, TranscodeOptions } from "./transcoder"
import { ok } from "assert"
import { decode as iniDecode } from "ini"
import formatNumber from "format-number"
import prettyMs from "pretty-ms"
import chalk from "chalk"

const percent1 = formatNumber({ round: 1, suffix: "%" })

export abstract class FfmpegTranscoder implements Transcoder {
  name = "<ffmpeg>"
  protected additionalFlags: string[] = []
  protected audioCodec: string = "libopus"

  abstract exclude(path: string): boolean | Promise<boolean>

  protected async getTotalDuration(path: string) {
    const totalDurationSecondsStr = (
      await execa("ffprobe", [
        ..."-v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1".split(
          " "
        ),
        path
      ])
    ).stdout.toString()
    const totalDuration = Number.parseInt(totalDurationSecondsStr) * 1000
    return totalDuration
  }

  protected abstract outputExtname(path: string): string

  async transcode(
    source: string,
    destination: string,
    { flags = [], progress = () => {} }: TranscodeOptions = {}
  ) {
    const totalDuration = await this.getTotalDuration(source)
    const p0 = await execa("ffmpeg", ["-version"])
    const ffmpegVersion = p0.stdout.match(/ffmpeg version (\S+)/)?.[1]
    progress(`Using version ${ffmpegVersion}`)
    const start = Date.now()
    const args = [
      "-i",
      source,
      "-acodec",
      this.audioCodec,
      ..."-hide_banner -y -nostdin -ss 0 -map_metadata 0:g -scodec copy -map 0 -progress pipe:1".split(
        " "
      ),
      ...this.additionalFlags,
      ...flags,
      "--",
      destination
    ]
    const p = execa("ffmpeg", args, {
      stdio: ["pipe", "pipe", "inherit"]
    })
    p.stdout!.on("data", (chunk: Buffer) =>
      this.reportProgress({ chunk, start, totalDuration, progress })
    )
    await p
  }

  rename(path: string): string {
    return path
  }

  protected reportProgress({
    chunk,
    start,
    totalDuration,
    progress,
    prefix
  }: {
    chunk: Buffer
    start: number
    totalDuration: number
    progress: (s: string) => void
    prefix?: string
  }) {
    ok(chunk instanceof Buffer)
    const chunkString = chunk.toString()
    const map = iniDecode(chunkString)
    if (Object.keys(map).length === 0) {
      return
    }
    const outTimeMs = Math.floor((map.out_time_ms || 0) / 1000)
    const spentDuration = Date.now() - start
    const completion = outTimeMs / totalDuration
    const remainingWork = totalDuration - outTimeMs
    const speedSoFar = outTimeMs / spentDuration
    const eta = remainingWork / speedSoFar
    if (!map.bitrate) {
      return
    }

    progress(
      `${prefix ? prefix + " " : ""}pos=${chalk.green(
        prettyMs(outTimeMs)
      )}/${chalk.blue(prettyMsOrInf(totalDuration))} (${chalk.green(
        percent1(completion * 100)
      )}) op=${chalk.green(prettyMs(spentDuration))} eta=${chalk.yellow(
        prettyMsOrInf(eta)
      )} frame=${chalk.green(map.frame)} fps=${chalk.green(
        map.fps
      )} bitrate=${chalk.green(map.bitrate.trim())} speed=${chalk.green(
        map.speed.trim()
      )}`
    )
  }
}

function prettyMsOrInf(n: number) {
  if (n === Infinity) {
    return "∞"
  }
  return prettyMs(n)
}
