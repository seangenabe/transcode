import { Transcoder } from "./transcoder"
import { extname, basename, join, dirname } from "path"
import execa = require("execa")
import { pipeline as _pipeline } from "stream"
import { file as temporaryFile } from "tempy"

export class OpusAnyTranscoder implements Transcoder {
  name = "Opus (Decode from any via ffmpeg)"

  exclude(path: string): boolean {
    return extname(path) === ".opus"
  }

  async transcode(source: string, destination: string) {
    const tempFlac = temporaryFile({ extension: ".flac" })
    await execa("ffmpeg", ["-i", source, tempFlac])
    await execa(
      "opusenc",
      [..."--vbr --comp 10".split(" "), tempFlac, destination],
      {
        stdout: "inherit",
        stderr: "inherit"
      }
    )
  }

  rename(path: string): string {
    return join(dirname(path), basename(path, extname(path)) + ".opus")
  }
}
