export interface Transcoder {
  name: string
  exclude(path: string): Promise<boolean> | boolean
  transcode(
    source: string,
    destination: string,
    options: TranscodeOptions
  ): Promise<void>
  rename(path: string): string
}

export interface TranscodeOptions {
  flags?: string[]
  progress?: (s: string) => void
}
