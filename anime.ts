import execa from "execa"
import { SvtAv1Transcoder } from "./svt-av1"

export class AnimeTranscoder extends SvtAv1Transcoder {
  name = "AV1 (anime mode)"

  additionalFlags =
    "-c:v libsvtav1 -crf 30 -preset 8 -g 240 -svtav1-params tune=0:scd=1".split(
      " "
    )

  async exclude(path: string) {
    const p = await execa("ffprobe", [
      ..."-v error -select_streams v:0 -show_entries stream=codec_name -of default=noprint_wrappers=1:nokey=1".split(
        " "
      ),
      path
    ])
    return p.stdout.includes("hevc") || p.stdout.includes("av1")
  }
}
